package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Admin;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;

public class AdminRegister {

	JFrame frame;
	JFrame parentFrame;
	private JTextField jtfUserName;
	private JPasswordField pfPasswd;
	private JPasswordField pfPasswd_2;

	

	/**
	 * Create the application.
	 */
	public AdminRegister(JFrame parentFrame) {
		initialize();
		this.parentFrame= parentFrame;
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//框架
		frame = new JFrame();
		frame.setTitle("\u7BA1\u7406\u5458\u6CE8\u518C");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---用户名
		JLabel label = new JLabel("\u7528 \u6237 \u540D\uFF1A");
		label.setBounds(55, 57, 72, 15);
		frame.getContentPane().add(label);
		
		//标签---密码
		JLabel label_1 = new JLabel("\u5BC6    \u7801\uFF1A");
		label_1.setBounds(55, 99, 72, 15);
		frame.getContentPane().add(label_1);
		
		//标签---确认密码
		JLabel label_2 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		label_2.setBounds(55, 137, 72, 15);
		frame.getContentPane().add(label_2);
		
		// 用户名文本框
		jtfUserName = new JTextField();
		jtfUserName.setBounds(163, 54, 168, 22);
		frame.getContentPane().add(jtfUserName);
		jtfUserName.setColumns(10);
		
		//用户密码框
		pfPasswd = new JPasswordField();
		pfPasswd.setBounds(163, 96, 168, 22);
		frame.getContentPane().add(pfPasswd);
		
		//确认密码框
		pfPasswd_2 = new JPasswordField();
		pfPasswd_2.setBounds(163, 134, 168, 22);
		frame.getContentPane().add(pfPasswd_2);
		
		//注册按钮
		JButton btnRegister = new JButton("\u6CE8\u518C");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Pattern namePattern = Pattern.compile("^[A-Za-z0-9]{3,20}$");
				Matcher nameMatcher = namePattern.matcher(jtfUserName.getText().trim());
				Pattern passPattern=null;
				Matcher passMatcher=null;
				String pass1 = new String(pfPasswd.getPassword());
				String pass2 = new String(pfPasswd_2.getPassword());
				if(pass1.equals(pass2)){
					passPattern = Pattern.compile("^[A-Za-z0-9]{6,20}$");
					passMatcher = passPattern.matcher(pass1);
					
					if(nameMatcher.matches()&&passMatcher.matches()){
						Admin admin = new Admin();
						admin.setAdminName(jtfUserName.getText().trim());
						admin.setAdminPass(pass1);
						File file = new File("D:/store/admin/"+jtfUserName.getText().trim()+".txt");
						try (
							ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));	
						){
							oos.writeObject(admin);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						};
						JOptionPane.showMessageDialog(null,
								"注册成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
						
					}else{
						JOptionPane.showMessageDialog(null,
								"用户名或密码不符合标准", "系统信息", JOptionPane.INFORMATION_MESSAGE);
						clear();
					}
					
				}else{
					JOptionPane.showMessageDialog(null,
							"两次输入密码不相同", "系统信息", JOptionPane.INFORMATION_MESSAGE);
					clear();
				}
				
			}
		});
		btnRegister.setBounds(267, 203, 93, 23);
		frame.getContentPane().add(btnRegister);
		
	}
	private void clear(){
		jtfUserName.setText("");
		pfPasswd.setText("");
		pfPasswd_2.setText("");
	}
	
}

package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.eclipse.osgi.container.Module.Settings;
import org.eclipse.swt.internal.mozilla.nsIBaseWindow;

import model.Good;
import vo.Store;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class AdminAdd {

	JFrame frame;
	private JTextField text_goodName;
	private JTextField text_goodPrice;
	private JTextField text_goodNum;
	private JTextField text_description;
	private Store store;
	private DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public AdminAdd(Store store,DefaultTableModel model) {
		initialize();
		this.store= store;
		this.model = model;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u6DFB\u52A0\u5546\u54C1");
		frame.setBounds(100, 100, 450, 316);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---商品名
		JLabel label = new JLabel("\u5546\u54C1\u540D\uFF1A");
		label.setBounds(41, 40, 54, 15);
		frame.getContentPane().add(label);
		
		//标签---单价
		JLabel label_1 = new JLabel("\u5355  \u4EF7\uFF1A");
		label_1.setBounds(41, 81, 54, 15);
		frame.getContentPane().add(label_1);
		
		//标签---数量
		JLabel label_2 = new JLabel("\u6570  \u91CF\uFF1A");
		label_2.setBounds(41, 126, 54, 15);
		frame.getContentPane().add(label_2);
		
		//标签---简介
		JLabel label_3 = new JLabel("\u7B80  \u4ECB\uFF1A");
		label_3.setBounds(41, 172, 54, 15);
		frame.getContentPane().add(label_3);
		
		//标签---单位
		JLabel label_4 = new JLabel("\u5355\u4F4D\uFF1A\u5143");
		label_4.setBounds(306, 81, 54, 15);
		frame.getContentPane().add(label_4);
		
		//文本框---商品名
		text_goodName = new JTextField();
		text_goodName.setBounds(136, 37, 221, 21);
		frame.getContentPane().add(text_goodName);
		text_goodName.setColumns(10);
		
		//文本框---单价
		text_goodPrice = new JTextField();
		text_goodPrice.setBounds(136, 78, 144, 21);
		frame.getContentPane().add(text_goodPrice);
		text_goodPrice.setColumns(10);
		
		//文本框---数量
		text_goodNum = new JTextField();
		text_goodNum.setBounds(136, 123, 221, 21);
		frame.getContentPane().add(text_goodNum);
		text_goodNum.setColumns(10);
		
		
		//文本框---简介
		text_description = new JTextField();
		text_description.setBounds(136, 169, 221, 43);
		frame.getContentPane().add(text_description);
		text_description.setColumns(10);
		
		//添加按钮
		JButton btn_add = new JButton("\u6DFB\u52A0");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Good good = new Good();
				good.setGoodName(text_goodName.getText().trim());
				good.setGoodPrice(Double.parseDouble(text_goodPrice.getText().trim()));
				good.setDescription(text_description.getText().trim());
				Integer number = Integer.parseInt(text_goodNum.getText().trim());
				store.add(good, number);
				Object[] objects = {good.getGoodId(),good.getGoodName(),good.getGoodPrice(),number};
				model.addRow(objects);
				store.saveStore(store);
				JOptionPane.showMessageDialog(null,
						"添加成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
				clear();
			}
		});
		btn_add.setBounds(295, 232, 93, 23);
		frame.getContentPane().add(btn_add);
	}
	
	/**
	 * @description : clear message in the TextFiled 
	 */
	public void clear(){
		text_description.setText("");
		text_goodName.setText("");
		text_goodNum.setText("");
		text_goodPrice.setText("");
	}
	
}

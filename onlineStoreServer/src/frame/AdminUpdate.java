package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import vo.Store;

import javax.management.loading.PrivateClassLoader;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminUpdate {

	JFrame frame;
	private JTextField tf_goodName;
	private JTextField tf_goodPrice;
	private JTextField tf_goodNum;
	private JTextField tf_goodDescription;
	private String goodName;
	private double goodPrice;
	private int goodNum;
	private String goodDescription;
	private Store store;

	

	/**
	 * Create the application.
	 */
	public AdminUpdate(String goodName,double goodPrice,int goodNum,Store store) {
		this.goodName = goodName;
		this.goodPrice = goodPrice;
		this.goodNum = goodNum;
		this.store = store;
		store.getStoreList().keySet().forEach((good)->{
			if(good.getGoodName().equals(goodName)){
				goodDescription = good.getDescription();
			}
		});
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u4FEE\u6539\u5546\u54C1");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---商品名称
		JLabel label = new JLabel("\u5546\u54C1\u540D\u79F0\uFF1A");
		label.setBounds(40, 44, 72, 15);
		frame.getContentPane().add(label);
		
		//标签---单价
		JLabel label_1 = new JLabel("\u5355    \u4EF7\uFF1A");
		label_1.setBounds(40, 86, 72, 15);
		frame.getContentPane().add(label_1);
		
		//标签---数量
		JLabel label_2 = new JLabel("\u6570    \u91CF\uFF1A");
		label_2.setBounds(40, 127, 72, 15);
		frame.getContentPane().add(label_2);
		
		//标签---简介
		JLabel label_3 = new JLabel("\u7B80    \u4ECB\uFF1A");
		label_3.setBounds(40, 166, 72, 15);
		frame.getContentPane().add(label_3);
		
		//文本框---商品名称
		tf_goodName = new JTextField();
		tf_goodName.setBounds(145, 41, 206, 21);
		tf_goodName.setText(goodName);
		tf_goodName.setEditable(false);
		frame.getContentPane().add(tf_goodName);
		tf_goodName.setColumns(10);
		
		//文本框---商品单价
		tf_goodPrice = new JTextField();
		tf_goodPrice.setBounds(145, 83, 133, 21);
		tf_goodPrice.setText(""+goodPrice);
		frame.getContentPane().add(tf_goodPrice);
		tf_goodPrice.setColumns(10);
		
		//文本框---商品数量
		tf_goodNum = new JTextField();
		tf_goodNum.setBounds(145, 124, 133, 21);
		tf_goodNum.setText(""+goodNum);
		frame.getContentPane().add(tf_goodNum);
		tf_goodNum.setColumns(10);
		
		//文本框---商品简介
		tf_goodDescription = new JTextField();
		tf_goodDescription.setBounds(145, 163, 206, 21);
		tf_goodDescription.setText(goodDescription);
		frame.getContentPane().add(tf_goodDescription);
		tf_goodDescription.setColumns(10);
		
		//更新按钮
		JButton btn_update = new JButton("\u4FEE\u6539");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				store.getStoreList().keySet().forEach((good)->{
					if(good.getGoodName().equals(goodName)){
						if(good.getGoodPrice()!=Double.parseDouble(tf_goodPrice.getText())){
							good.setGoodPrice(Double.parseDouble(tf_goodPrice.getText()));
							store.getStoreList().put(good, goodNum);
						}
						if(store.getStoreList().get(good)!=Integer.parseInt(tf_goodNum.getText())){
							store.getStoreList().put(good,Integer.parseInt(tf_goodNum.getText()));
						}
						
						Store.saveStore(store);
						JOptionPane.showMessageDialog(null,
								"修改成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
					}
				});
				
			}
		});
		btn_update.setBounds(284, 215, 93, 23);
		frame.getContentPane().add(btn_update);
	}
}

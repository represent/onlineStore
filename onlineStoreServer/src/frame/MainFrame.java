package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.eclipse.swt.widgets.Table;

import model.Admin;
import model.Good;
import server.TCPImpl;
import vo.Store;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class MainFrame {

	JFrame frame;
	JFrame parentFrame;
	private JLabel label_1;
	private JTextField textField_search;
	private JButton btn_search;
	private JButton btn_add;
	private JButton btn_update;
	private JButton btn_delete;
	//表格信息
	DefaultTableModel model;
	JTable table;
	JScrollPane scrollPane;
	//当前管理员
	Admin currentAdmin;
	//商店
	Store store;
	Good temp ;



	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public MainFrame(JFrame parentFrame,Admin admin) {
		setStore();
		this.parentFrame=parentFrame;
		this.currentAdmin= admin;
		initialize();
	}
	
	/**
	 * @description : make the store get the new message 
	 */
	public void setStore(){
		File file = new File("D:/store/store.txt");
		try(
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
		){
			store = (Store)ois.readObject();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//添加框架
		frame = new JFrame();
		frame.setTitle("\u4E2D\u5317\u5927\u5B66\u5728\u7EBF\u5546\u5E97\u540E\u53F0\u7BA1\u7406\u7CFB\u7EDF");
		frame.setBounds(100, 100, 718, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签：当前管理员
		JLabel label = new JLabel("\u5F53\u524D\u7BA1\u7406\u5458\uFF1A");
		label.setBounds(10, 10, 132, 15);
		frame.getContentPane().add(label);
		JLabel lblNewLabel = new JLabel(currentAdmin.getAdminName());
		lblNewLabel.setBounds(88, 10, 54, 15);
		frame.getContentPane().add(lblNewLabel);
		
		//退出登录
		JButton btnExit = new JButton("\u9000\u51FA\u7CFB\u7EDF");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TCPImpl.userList.clear();
				frame.setVisible(false);
				currentAdmin= null;
				parentFrame.setVisible(true);
			}
		});
		btnExit.setBounds(582, 6, 93, 23);
		frame.getContentPane().add(btnExit);
		
		//标签  商品名称
		label_1 = new JLabel("\u5546\u54C1\u540D\u79F0\uFF1A");
		label_1.setBounds(10, 58, 66, 15);
		frame.getContentPane().add(label_1);
		//搜索框
		textField_search = new JTextField();
		textField_search.setBounds(76, 55, 80, 21);
		frame.getContentPane().add(textField_search);
		textField_search.setColumns(10);
		//搜索按钮
		btn_search = new JButton("\u641C\u7D22");
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//将model 中信息移除
				int length = model.getRowCount();
				for(int i = length-1;i>=0;i--){
					model.removeRow(i);
				}
				
				
				//获取临时选择结果
				String search = textField_search.getText().trim();
				Set<Good> goods = store.getStoreList().keySet();
				Map<Good,Integer> temp = new HashMap<>();
				goods.forEach((good)->{
					if((good.getGoodName().indexOf(search))!=-1){
						temp.put(good, store.getStoreList().get(good));
					}
				});
				
				
				Set<Good> tempGoods = temp.keySet();
				tempGoods.forEach((good)->{
					Object [] object = new Object[4];
					object[0]=good.getGoodId();
					object[1]=good.getGoodName();
					object[2]=good.getGoodPrice();
					object[3]=temp.get(good);
					model.addRow(object);
				});
				
				 
			}
		});
		btn_search.setBounds(166, 54, 93, 23);
		frame.getContentPane().add(btn_search);
		
		//添加
		btn_add = new JButton("\u6DFB\u52A0\u5546\u54C1");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							AdminAdd window = new AdminAdd(store,model);
							window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
							window.frame.setVisible(true);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btn_add.setBounds(393, 54, 93, 23);
		frame.getContentPane().add(btn_add);
		
		//修改
		btn_update = new JButton("\u4FEE\u6539\u5546\u54C1");
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							 int selectedRow = table.getSelectedRow();//获得选中行的索引
							 if(selectedRow!=-1){
								String goodName = (String)table.getValueAt(selectedRow, 1);
								double goodPrice = (double)table.getValueAt(selectedRow,2);
								int goodNum = (int)table.getValueAt(selectedRow,3);
								AdminUpdate window = new AdminUpdate(goodName,goodPrice,goodNum,store);
								window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
								window.frame.setVisible(true);
							 }
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btn_update.setBounds(496, 54, 93, 23);
		frame.getContentPane().add(btn_update);
		
		//删除
		btn_delete = new JButton("\u5220\u9664\u5546\u54C1");
		
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 int selectedRow = table.getSelectedRow();//获得选中行的索引
                if(selectedRow!=-1)  //存在选中行
                {
                	
                    String name = (String)table.getValueAt(selectedRow,1);
                    Set<Good> goods = store.getStoreList().keySet();
                   
                    goods.forEach((good)->{
                    	if(good.getGoodName().equals(name)){
                    		temp = good;
                    	}
                    });
                    int option = JOptionPane.showConfirmDialog(null,
                    	       "删除文件", "保存删除?", JOptionPane.YES_NO_OPTION,
                    	       JOptionPane.WARNING_MESSAGE, null);
            	    switch (option) {
	            	     case JOptionPane.YES_NO_OPTION: {
	            	    	 store.getStoreList().remove(temp);
	                         Store.saveStore(store);
	                         //删除行
	                         model.removeRow(selectedRow);
	            	      break;
	            	     }
	            	     case JOptionPane.NO_OPTION:
	            	      break;
            	    }
                }
			}
		});
		btn_delete.setBounds(599, 54, 93, 23);
		frame.getContentPane().add(btn_delete);
		
		//列表信息
		model = new DefaultTableModel();
		model.addColumn("商品编号");
		model.addColumn("名称");
		model.addColumn("单价(人民币)");
		model.addColumn("库存");
		setTable();
		table = new JTable( model );
		
		
		//滚动匡添加信息
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 87, 682, 348);
		frame.getContentPane().add(scrollPane);
		
		JButton btn_firstPage = new JButton("\u9996\u9875");
		btn_firstPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTable();
			}
		});
		btn_firstPage.setBounds(290, 54, 93, 23);
		frame.getContentPane().add(btn_firstPage);
		
		//标签---当前在线人数-提示
		JLabel label_login_numTip = new JLabel("\u5F53\u524D\u5728\u7EBF\u4EBA\u6570\uFF1A");
		label_login_numTip.setBounds(281, 10, 102, 15);
		frame.getContentPane().add(label_login_numTip);
		
		//标签---当前在线人数
		JLabel label_login_num = new JLabel("0");
		label_login_num.setEnabled(false);
		label_login_num.setBounds(382, 10, 54, 15);
		frame.getContentPane().add(label_login_num);
		//线程---实时更新在线人数
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					try {
						Thread.sleep(1000);
					} catch (Exception e1) {
						// TODO: handle exception
					}
					label_login_num.setText(""+TCPImpl.userList.size());
				}
			}
		}).start();
		
		

		
		
	}
	/**
	 * @description : add table to frame 
	 */
	public void setTable(){
		File file = new File("D:/store/store.txt");
		try(
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
		){
			store = (Store)ois.readObject();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//将model 中信息移除
		int length = model.getRowCount();
		for(int i = length-1;i>=0;i--){
			model.removeRow(i);
		}
		
		Set<Good> goods = store.getStoreList().keySet();
		goods.forEach((good)->{
			Object [] object = new Object[4];
			object[0]=good.getGoodId();
			object[1]=good.getGoodName();
			object[2]=good.getGoodPrice();
			object[3]=store.getStoreList().get(good);
			model.addRow(object);
		});
		
	}
}

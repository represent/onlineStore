package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.omg.CORBA.Current;

import com.ibm.icu.util.Currency;

import model.Admin;
import model.Good;
import server.TCPServer;
import vo.Store;

import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;

public class AdminLogin {

	private JFrame frame;
	private JTextField jtfUserName;
	private JPasswordField jtfPasswd;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminLogin window = new AdminLogin();
					window.frame.setVisible(true);
					TCPServer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminLogin() {
		initialize();
		//初始化文件夹与相关文件
		File file1 = new File("D:/store/");
		File file2 =  new File("D:/store/admin/");
		File file3 = new File("D:/store/goods/");
		File file4 = new File("D:/store/user/");
		File file5 = new File("D:/store/store.txt");
		if(!file1.exists()){
			file1.mkdirs();
		}
		if(!file2.exists()){
			file2.mkdirs();
		}
		if(!file3.exists()){
			file3.mkdirs();
		}
		if(!file4.exists()){
			file4.mkdirs();
		}
		if(!file5.exists()){
			Store store = Store.getInstance();
			try(
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file5));	
			){
				oos.writeObject(store);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//框架
		frame = new JFrame();
		frame.setTitle("\u4E2D\u5317\u5927\u5B66\u5728\u7EBF\u5546\u5E97\u7BA1\u7406\u7CFB\u7EDF-\u7BA1\u7406\u5458\u767B\u9646");
		frame.setBounds(100, 100, 521, 383);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//标签----用户名
		JLabel label = new JLabel("\u7528\u6237\u540D\uFF1A");
		label.setBounds(73, 175, 54, 15);
		frame.getContentPane().add(label);
		
		//标签----密码
		JLabel label_1 = new JLabel("\u5BC6  \u7801\uFF1A");
		label_1.setBounds(73, 219, 54, 15);
		frame.getContentPane().add(label_1);
		
		//用户名文本框
		jtfUserName = new JTextField();
		jtfUserName.setBounds(179, 172, 224, 22);
		frame.getContentPane().add(jtfUserName);
		jtfUserName.setColumns(10);
		
		//密码文本框
		jtfPasswd = new JPasswordField();
		jtfPasswd.setBounds(179, 216, 224, 22);
		frame.getContentPane().add(jtfPasswd);
		
		//登录按钮
		JButton btnLogin = new JButton("\u767B\u9646");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							//get the admin's information
							String name = jtfUserName.getText().trim();
							String pass = new String(jtfPasswd.getPassword());
							File file = new File("D:/store/admin/"+name+".txt");
							if(file.exists()){
								ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
								Admin admin = (Admin)ois.readObject();
								ois.close();
								if(pass.equals(admin.getAdminPass())){
									MainFrame window = new MainFrame(frame,admin);
									
									window.frame.setVisible(true);
									
									frame.setVisible(false);
									clear();
								}else{
									JOptionPane.showMessageDialog(null,
											"密码错误", "系统信息", JOptionPane.INFORMATION_MESSAGE);
									clear();
								}
								
							}else{
								JOptionPane.showMessageDialog(null,
										"用户不存在", "系统信息", JOptionPane.INFORMATION_MESSAGE);
								clear();
							}
							
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnLogin.setBounds(300, 274, 93, 23);
		frame.getContentPane().add(btnLogin);
		
		//注册按钮
		JButton btnRegister = new JButton("\u6CE8\u518C");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							AdminRegister window = new AdminRegister(frame);
							window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnRegister.setBounds(94, 274, 93, 23);
		frame.getContentPane().add(btnRegister);
		
		JLabel lblNewLabel = new JLabel("");
		ImageIcon icon= new ImageIcon("image/admin.jpg");
		lblNewLabel.setIcon(icon);
		lblNewLabel.setBounds(189, 49, 118, 93);
		frame.getContentPane().add(lblNewLabel);
	}
	/**
	 * @description : clear message that user keyboard input 
	 */
	private void clear(){
		jtfPasswd.setText("");
		jtfUserName.setText("");
	}
}

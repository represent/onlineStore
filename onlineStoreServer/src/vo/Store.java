package vo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import model.Good;

public class Store implements Serializable{
	private static Store store ;
	private Map<Good,Integer> storeList = new HashMap<>();
	
	public static Store getInstance(){
		if(store == null){
			store = new Store();
		}
		return store; 
	}
	public void add(Good good,Integer number){
		storeList.put(good,number);
	}
	public Map<Good, Integer> getStoreList() {
		return storeList;
	}
	public void setStoreList(Map<Good, Integer> storeList) {
		this.storeList = storeList;
	}
	/**
	 * @description : write the information that store have to the file
	 * @param : Store store 
	 */
	public static  void saveStore(Store store){
		File file = new File("D:/store/store.txt");
		if(file.exists()){
			file.delete();
			try(
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			){
				oos.writeObject(store);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	}
}

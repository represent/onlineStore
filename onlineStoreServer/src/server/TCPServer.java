package server;

import java.util.List;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class TCPServer implements Runnable {

	private static ServerSocket serverSocket ;
	private static volatile boolean flag = true;
	public static List<Socket> sockets = new ArrayList<>();

	public TCPServer(){
		try {
			serverSocket  = new ServerSocket(9999);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @description : 处理接收到的客户端的处理信息 
	 * 
	 */
	@Override
	public void run(){
		while(flag){
			try {
				Socket socket = serverSocket.accept();
				if(!sockets.contains(socket)){
					sockets.add(socket);
				}
				sockets.forEach((temp)->{
					if(!temp.isConnected()){
						sockets.remove(temp);
					}
				});
				//***************************************************
				System.out.println("listening......");
				new Thread(new TCPImpl(socket)).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * @description : 启动线程 并设置为守护线程 
	 */
	public static void start(){
		Thread tcpServer = new Thread(new TCPServer());
		tcpServer.setDaemon(true);
		tcpServer.start();
	}
	/**
	 * @description : 将循环变量flag 赋值为false 让run中while循环结束 
	 */
	public static void stop(){
		flag=false;
		if(serverSocket!=null && !serverSocket.isClosed()){
			try {
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

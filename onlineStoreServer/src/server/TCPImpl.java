package server;

import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




import model.Good;
import model.OrderModel;
import model.SendModel;
import model.User;
import vo.Store;

public class TCPImpl  implements Runnable{
	private Socket socket;
	private Good goodTemp;
	private int numTemp;
	public static List<User> userList = new ArrayList<>();
	public TCPImpl(Socket socket){
		this.socket = socket;
	}
	@Override
	public void run(){
		try(
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		){
			SendModel model = (SendModel)ois.readObject();
			switch(model.getType()){
				case "login":{
					User user = (User)model.getObject();
					File file = new File("D:/store/user/"+user.getUserName()+".txt");
					if(file.exists()){
						User userChecked =(User)new ObjectInputStream(new FileInputStream(file)).readObject() ;
						if(user.getUserPass().equals(userChecked.getUserPass())){
							if(!userList.contains(user)){
								SendModel result = new SendModel();
								result.setType("success@@登录成功");
								userList.add(user);
								result.setObject(userChecked);
								try {
									oos.writeObject(result);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else{
								SendModel result = new SendModel();
								result.setType("error@@用户已经在线 ");
								try {
									oos.writeObject(result);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
						}else{
							SendModel result = new SendModel();
							result.setType("error@@密码不正确");
							oos.writeObject(result);
						}
					}else{
						SendModel result = new SendModel();
						result.setType("error@@用户不存在");
						oos.writeObject(result);
					}
					
					break;
					}
				case "register":{
					User user = (User)model.getObject();
					File file = new File("D:/store/user/"+user.getUserName()+".txt");
					if(!file.exists()){
						//将注册信息写入文件
						ObjectOutputStream tempFile = new ObjectOutputStream(new FileOutputStream(file));
						tempFile.writeObject(user);
						SendModel result = new SendModel();
						result.setType("success@@注册成功");
						oos.writeObject(result);
					}else{
						SendModel result = new SendModel();
						result.setType("error@@用户存在");
						oos.writeObject(result);
					}
					break;
				}
				case "search" :{
					//获取传过来的货物信息  如果为空  则表示查询全部 否则按货物名查询
					Good good = (Good)model.getObject();
					//得到商店中的货物
					File file = new File("d:/store/store.txt");
					Store store=(Store)new ObjectInputStream(new FileInputStream(file)).readObject(); 
					if(good==null){
						SendModel result = new SendModel();
						result.setType("success");
						result.setObject(store.getStoreList());
						oos.writeObject(result);
					}else{
						SendModel result = new SendModel();
						result.setType("success");
						Map<Good,Integer> temp = new HashMap<>();
						String search = good.getGoodName();
						store.getStoreList().keySet().forEach((tempGood)->{
							if(tempGood.getGoodName().indexOf(search)!=-1){
								temp.put(tempGood,store.getStoreList().get(tempGood));
							}
						});
						result.setObject(temp);
						oos.writeObject(result);
					}
					break;
				}
				case "addGood":{
					OrderModel orderModel = (OrderModel)model.getObject();
					String userName = orderModel.getUserName();
					String goodName = orderModel.getGoodName();
					int goodNumber = orderModel.getGoodNum(); 
					File file = new File("d:/store/user/"+userName+".txt");
					User user = (User)new ObjectInputStream(new FileInputStream(file)).readObject();
					File file2 = new File("d:/store/store.txt");
					Store store = (Store)new ObjectInputStream(new FileInputStream(file2)).readObject();
					store.getStoreList().keySet().forEach((good)->{
						if(good.getGoodName().equals(goodName)){
							int remain = store.getStoreList().get(good)-goodNumber;
							if(remain>0){
								user.add(good, goodNumber);
								store.getStoreList().put(good,remain);
								User.saveUser(user);
								Store.saveStore(store);
								
								
								SendModel result = new SendModel();
								result.setType("success");
								result.setObject(user);
								try {
									oos.writeObject(result);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else{
								SendModel result = new SendModel();
								result.setType("error@@库存不足");
								try {
									oos.writeObject(result);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							
						
						}
					});
						
					break;
				}
				case "deleteGood":{
					OrderModel orderModel = (OrderModel)model.getObject();
					String goodName = orderModel.getGoodName();
					String userName = orderModel.getUserName();
					File file = new File("d:/store/user/"+userName+".txt");
					User user = (User)new ObjectInputStream(new FileInputStream(file)).readObject();
					File file2 = new File("d:/store/store.txt");
					Store store = (Store)new ObjectInputStream(new FileInputStream(file2)).readObject();
					user.getGoodCar().keySet().forEach((good)->{
						if(good.getGoodName().equals(goodName)){
							goodTemp = good;
							numTemp = user.getGoodCar().get(good);
						}
					});
					
					store.getStoreList().keySet().forEach((good)->{
						if(good.getGoodName().equals(goodName)){
							store.getStoreList().put(good,store.getStoreList().get(good)+numTemp);
						}
					});
					
					if(user.getGoodCar().remove(goodTemp)!=null){
						SendModel result = new SendModel();
						result.setType("success");
						result.setObject(user);
						try {
							oos.writeObject(result);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						User.saveUser(user);
						Store.saveStore(store);
					}else{
						SendModel result = new SendModel();
						result.setType("error@@该货物不存在");
						oos.writeObject(result);
					}
					
					break;
				}
				case "pay":{
					OrderModel orderModel = (OrderModel)model.getObject();
					String userName = orderModel.getUserName();
					File file = new File("d:/store/user/"+userName+".txt");
					User user = (User)new ObjectInputStream(new FileInputStream(file)).readObject();
					user.addRecord(user.getGoodCar());
					user.getGoodCar().clear();
					SendModel result = new SendModel();
					result.setType("success");
					result.setObject(user);
					User.saveUser(user);
					oos.writeObject(result);
					break;
				}
				case "logout":{
					User user = (User)model.getObject();
					userList.forEach((userTemp)->{
						if(userTemp.getUserName().equals(user.getUserName())){
							userList.remove(userTemp);
						}
					});
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}

package model;

import java.io.Serializable;
import java.util.UUID;




public class Good implements Serializable{
	private UUID goodId =UUID.randomUUID();  
	private String goodName;
	private double goodPrice;
	private String description;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public UUID getGoodId() {
		return goodId;
	}
	public void setGoodId(UUID goodId) {
		this.goodId = goodId;
	}
	public String getGoodName() {
		return goodName;
	}
	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}
	public double getGoodPrice() {
		return goodPrice;
	}
	public void setGoodPrice(double goodPrice) {
		this.goodPrice = goodPrice;
	}
	@Override
	public int hashCode() {
		return goodName.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Good other = (Good) obj;
		if (!goodName.equals(other.goodName))
			return false;
		return true;
	}
	
	//根据需要重写toString()
	@Override
	public String toString() {
		return "Goods [gid=" + goodId + ", goodsName=" + goodName + ", price=" +goodPrice+ "]";
	}
	
}

package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.eclipse.osgi.internal.permadmin.SecurityTable;

import model.Good;
import model.SendModel;
import model.User;

import javax.management.loading.PrivateClassLoader;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.awt.event.ActionEvent;

public class MainFrame {

	JFrame frame;
	private JFrame parentFrame;
	private JTextField tf_goodName;
	private DefaultTableModel model;
	private JTable table;
	private JScrollPane scrollPane;
	static User currentUser;
	private int sum;


	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public MainFrame(JFrame parentFrame,User user)  {
		this.currentUser = user;
		this.parentFrame = parentFrame;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u4E2D\u5317\u5927\u5B66\u5728\u7EBF\u7F51\u5E97---\u5BA2\u6237\u7AEF");
		frame.setBounds(100, 100, 685, 462);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---购物车
		JLabel label_car = new JLabel("\u8D2D\u7269\u8F66\uFF1A");
		label_car.setBounds(10, 10, 54, 15);
		frame.getContentPane().add(label_car);
		
		//标签---购物车
		JLabel label_car2 = new JLabel("\u4EF6\u5546\u54C1");
		label_car2.setBounds(82, 10, 54, 15);
		frame.getContentPane().add(label_car2);
		
		//标签---商品数量
		JLabel label_carNum = new JLabel();
		label_carNum.setEnabled(false);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				while(true){
					sum = 0;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					currentUser.getGoodCar().keySet().forEach((tempGood)->{
						sum+= currentUser.getGoodCar().get(tempGood);
					});
					label_carNum.setText(""+sum);
				}
			
			}
		}).start();
			
		label_carNum.setBounds(61, 10, 21, 15);
		frame.getContentPane().add(label_carNum);
		
		//按钮---进入购物车
		JButton btn_car = new JButton("\u67E5\u770B\u8D2D\u7269\u8F66");
		btn_car.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							GoodCar window = new GoodCar(currentUser);
							window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btn_car.setBounds(130, 6, 120, 23);
		frame.getContentPane().add(btn_car);
		
		//按钮---退出系统
		JButton btn_exit = new JButton("\u9000\u51FA\u767B\u5F55");
		btn_exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try(
					Socket socket = new Socket("127.0.0.1",9999);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				){
					SendModel sendModel  = new SendModel();
					sendModel.setType("logout");
					sendModel.setObject(currentUser);
					oos.writeObject(sendModel);
					User.saveUser(currentUser);
					currentUser=null;
					parentFrame.setVisible(true);
					frame.setVisible(false);
				}catch(Exception e1){
					e1.printStackTrace();
				}
				
			}
		});
		btn_exit.setBounds(566, 6, 93, 23);
		frame.getContentPane().add(btn_exit);
		
		//标签---商品名-搜索
		JLabel label_goodName = new JLabel("\u5546\u54C1\u540D\u79F0\uFF1A");
		label_goodName.setBounds(10, 49, 72, 15);
		frame.getContentPane().add(label_goodName);
		
		//文本框---商品名-搜索
		tf_goodName = new JTextField();
		tf_goodName.setBounds(82, 46, 93, 21);
		frame.getContentPane().add(tf_goodName);
		tf_goodName.setColumns(10);
		
		//按钮---搜索
		JButton btn_search = new JButton("\u641C\u7D22");
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String goodName = tf_goodName.getText().trim();
				setTable("search", goodName);
			}
		});
		btn_search.setBounds(190, 45, 93, 23);
		frame.getContentPane().add(btn_search);
		
		//按钮 ---显示商品详细信息
		JButton btn_goodInfo = new JButton("\u67E5\u770B\u5546\u54C1\u8BE6\u7EC6\u4FE1\u606F");
		btn_goodInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						int selectedRow = table.getSelectedRow();//获得选中行的索引
						if(selectedRow!=-1){
							String goodName = (String)table.getValueAt(selectedRow, 1);
							try {
								GoodInfo window = new GoodInfo(goodName,currentUser);
								window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
								window.frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}else{
							JOptionPane.showMessageDialog(null,
									"未选中商品，请双击选中", "系统信息", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
			}
		});
		btn_goodInfo.setBounds(462, 45, 197, 23);
		frame.getContentPane().add(btn_goodInfo);
		
		//按钮 ---刷新界面
		JButton btn_firstPage = new JButton("\u9996\u9875");
		btn_firstPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTable("search");
			}
		});
		btn_firstPage.setBounds(329, 45, 93, 23);
		frame.getContentPane().add(btn_firstPage);
		
		//scrollPane 中table添加的模型
		model = new DefaultTableModel();
		model.addColumn("商品编号");
		model.addColumn("名称");
		model.addColumn("单价(人民币)");
		model.addColumn("库存");
		setTable("search");
		table = new JTable( model );
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 74, 649, 339);
		frame.getContentPane().add(scrollPane);
		
		//标签---当前用户-提示
		JLabel label_userTip = new JLabel("\u5F53\u524D\u7528\u6237\uFF1A");
		label_userTip.setBounds(329, 10, 72, 15);
		frame.getContentPane().add(label_userTip);
		
		//标签---存放当前用户
		JLabel label_user = new JLabel("New label");
		label_user.setEnabled(false);
		label_user.setBounds(402, 10, 80, 15);
		if(currentUser!=null){
			label_user.setText(currentUser.getUserName());
		}else{
			label_user.setText("");
		}
		frame.getContentPane().add(label_user);
		
		
	}
	public void setTable(String searchType){
		try(
			Socket socket = new Socket("127.0.0.1",9999);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		){
			SendModel sendModel = new SendModel();
			sendModel.setType(searchType);
			sendModel.setObject(null);
			
			oos.writeObject(sendModel);
			
			SendModel result = (SendModel)ois.readObject();
			String[] message = result.getType().split("@@");
			if(message[0].equalsIgnoreCase("success")){
				int length = model.getRowCount();
				for(int i = length-1;i>=0;i--){
					model.removeRow(i);
				}
				Map<Good,Integer> tempTable =(Map<Good, Integer>)result.getObject();
				tempTable.keySet().forEach((good)->{
					Object [] object = new Object[4];
					object[0]=good.getGoodId();
					object[1]=good.getGoodName();
					object[2]=good.getGoodPrice();
					object[3]=tempTable.get(good);
					model.addRow(object);
				});
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setTable(String searchType , String goodName){
		try(
				Socket socket = new Socket("127.0.0.1",9999);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			){
				SendModel sendModel = new SendModel();
				sendModel.setType(searchType);
				Good tempGood = new Good();
				tempGood.setGoodName(goodName);
				sendModel.setObject(tempGood);
				
				
				oos.writeObject(sendModel);
				
				SendModel result = (SendModel)ois.readObject();
				String[] message = result.getType().split("@@");
				if(message[0].equalsIgnoreCase("success")){
					int length = model.getRowCount();
					for(int i = length-1;i>=0;i--){
						model.removeRow(i);
					}
					Map<Good,Integer> tempTable =(Map<Good, Integer>)result.getObject();
					tempTable.keySet().forEach((good)->{
						Object [] object = new Object[4];
						object[0]=good.getGoodId();
						object[1]=good.getGoodName();
						object[2]=good.getGoodPrice();
						object[3]=tempTable.get(good);
						model.addRow(object);
					});
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}

package frame;

import java.awt.EventQueue;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Good;
import model.OrderModel;
import model.SendModel;
import model.User;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GoodInfo {

	JFrame frame;
	private JTextField tf_goodNum;
	private String goodName;
	private JLabel label_storeGoodsNum;
	private JLabel label_goodDescription;
	private JLabel label_goodPrice;
	private JLabel label_goodName;
	private User currentUser;
	

	

	/**
	 * Create the application.
	 */
	public GoodInfo(String goodName,User currentUser) {
		this.goodName = goodName;
		this.currentUser = currentUser;
		initialize();
		setTable("search",goodName);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u5546\u54C1\u8BE6\u7EC6\u4FE1\u606F");
		frame.setBounds(100, 100, 478, 415);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---货物名称-提示
		JLabel label_goodNameTip = new JLabel("\u5546\u54C1\u540D\u79F0\uFF1A");
		label_goodNameTip.setBounds(43, 48, 71, 15);
		frame.getContentPane().add(label_goodNameTip);
		
		//标签---货物名称
		label_goodName = new JLabel("new lable");
		label_goodName.setEnabled(false);
		label_goodName.setBounds(131, 48, 141, 15);
		frame.getContentPane().add(label_goodName);
		
		//标签---货物价格-提示
		JLabel label_goodPriceTip = new JLabel("\u5355    \u4EF7\uFF1A");
		label_goodPriceTip.setBounds(42, 92, 72, 15);
		frame.getContentPane().add(label_goodPriceTip);
		
		//标签---货物价格
		label_goodPrice = new JLabel("New label");
		label_goodPrice.setEnabled(false);
		label_goodPrice.setBounds(131, 92, 141, 15);
		frame.getContentPane().add(label_goodPrice);
		
		//标签---货物价格-提示二
		JLabel label_goodPriceTip2 = new JLabel("[\u5355\u4F4D\uFF1A\u5143]");
		label_goodPriceTip2.setBounds(264, 92, 73, 15);
		frame.getContentPane().add(label_goodPriceTip2);
		
		//标签---货物详细介绍-提示
		JLabel label_goodDescriptTip = new JLabel("\u7B80    \u4ECB\uFF1A");
		label_goodDescriptTip.setBounds(43, 138, 71, 15);
		frame.getContentPane().add(label_goodDescriptTip);
		
		//标签---货物详细介绍
		label_goodDescription = new JLabel("New label");
		label_goodDescription.setEnabled(false);
		label_goodDescription.setBounds(131, 138, 233, 15);
		frame.getContentPane().add(label_goodDescription);
		
		//标签---货物数量-提示
		JLabel label_goodNumTip = new JLabel("\u8D2D\u4E70\u6570\u91CF\uFF1A");
		label_goodNumTip.setBounds(43, 234, 71, 15);
		frame.getContentPane().add(label_goodNumTip);
		
		//文本框---货物数量
		tf_goodNum = new JTextField();
		tf_goodNum.setText("1");
		tf_goodNum.setBounds(131, 231, 66, 21);
		frame.getContentPane().add(tf_goodNum);
		tf_goodNum.setColumns(10);
		
		//按钮 ---将货物添加到购物车中
		JButton btn_addGood = new JButton("\u52A0\u5165\u8D2D\u7269\u8F66");
		btn_addGood.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addGood(label_goodName.getText().trim(),Integer.parseInt(tf_goodNum.getText().trim()),currentUser);
			}
		});
		btn_addGood.setBounds(264, 230, 122, 23);
		frame.getContentPane().add(btn_addGood);
		
		//标签---库存量-提示
		JLabel lable_storeGoodsNumTip = new JLabel("\u5E93    \u5B58\uFF1A");
		lable_storeGoodsNumTip.setBounds(43, 295, 71, 15);
		frame.getContentPane().add(lable_storeGoodsNumTip);
		
		//标签---库存量
		label_storeGoodsNum = new JLabel("New label");
		label_storeGoodsNum.setEnabled(false);
		label_storeGoodsNum.setBounds(131, 295, 117, 15);
		frame.getContentPane().add(label_storeGoodsNum);
	}
	public void setTable(String searchType , String goodName){
		try(
				Socket socket = new Socket("127.0.0.1",9999);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			){
				SendModel sendModel = new SendModel();
				sendModel.setType(searchType);
				Good tempGood = new Good();
				tempGood.setGoodName(goodName);
				sendModel.setObject(tempGood);
				
				
				oos.writeObject(sendModel);
				
				SendModel result = (SendModel)ois.readObject();
				String[] message = result.getType().split("@@");
				if(message[0].equalsIgnoreCase("success")){
					Map<Good,Integer> tempTable =(Map<Good, Integer>)result.getObject();
					tempTable.keySet().forEach((good)->{
						label_goodName.setText(good.getGoodName());;
						label_goodPrice.setText(""+good.getGoodPrice()); ;
						label_storeGoodsNum.setText(""+tempTable.get(good));
						label_goodDescription.setText(good.getDescription());
						
					});
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void addGood(String goodName,int num,User user){
		try(
				Socket socket = new Socket("127.0.0.1",9999);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());	
		){
				OrderModel orderModel = new OrderModel();
				orderModel.setGoodName(label_goodName.getText().trim());
				orderModel.setGoodNum(Integer.parseInt(tf_goodNum.getText().trim()));
				orderModel.setType("addGood");
				orderModel.setUserName(currentUser.getUserName());
				SendModel sendModel = new SendModel();
				sendModel.setType("addGood");
				sendModel.setObject(orderModel);
				oos.writeObject(sendModel);
				
				SendModel result = (SendModel)ois.readObject();
				 
				String message[] = result.getType().split("@@");
				if(message[0].equalsIgnoreCase("success")){
					MainFrame.currentUser=(User)result.getObject();
					JOptionPane.showMessageDialog(null,
							"添加成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
				}else{
					JOptionPane.showMessageDialog(null,
							"添加失败", "系统信息", JOptionPane.INFORMATION_MESSAGE);
				}
				
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}

package frame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.SendModel;
import model.User;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class UserLogin {

	private JFrame frame;
	private JTextField tf_userName;
	private JPasswordField tf_passwd;
	private JLabel label_error_tip;
	private Socket socket;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserLogin window = new UserLogin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserLogin() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u4E2D\u88AB\u5927\u5B66\u5728\u7EBF\u5546\u5E97---\u7528\u6237\u767B\u5F55");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//标签---图片
		JLabel user_photo = new JLabel("");
		user_photo.setBounds(183, 25, 114, 78);
		ImageIcon icon= new ImageIcon("image/user.jpg");
		user_photo.setIcon(icon);
		frame.getContentPane().add(user_photo);
		//标签---用户名
		JLabel label_userName = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_userName.setBounds(68, 116, 54, 15);
		frame.getContentPane().add(label_userName);
		
		//标签---密码
		JLabel label_passwd = new JLabel("\u5BC6  \u7801\uFF1A");
		label_passwd.setBounds(68, 160, 54, 15);
		frame.getContentPane().add(label_passwd);
		
		//文本框---用户名
		tf_userName = new JTextField();
		tf_userName.setBounds(167, 113, 172, 21);
		frame.getContentPane().add(tf_userName);
		tf_userName.setColumns(10);
		
		//文本框---密码框
		tf_passwd = new JPasswordField();
		tf_passwd.setBounds(167, 157, 172, 21);
		frame.getContentPane().add(tf_passwd);
		tf_passwd.setColumns(10);
		
		//按钮---登录
		JButton btn_login = new JButton("\u767B\u5F55");
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					boolean flag = true;
					public void run() {
						try {
							socket = new Socket("127.0.0.1",9999);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						try (
								ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
								ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());	
							){
							
							while(flag){
								//获取文本信息
								String userName = tf_userName.getText().trim();
								String userPass = new String(tf_passwd.getPassword());
								if(userName==null||userName.equals("")){
									label_error_tip.setText("用户名不为空");
									return;
								}else if(userPass==null||userPass.equals("")){
									label_error_tip.setText("密码不为空");
									return ;
								}
							
								SendModel sendModel = new SendModel();
								sendModel.setType("login");
								User user = new User();
								user.setUserName(userName);
								user.setUserPass(userPass);
								sendModel.setObject(user);
								oos.writeObject(sendModel);
								
								SendModel result = (SendModel)ois.readObject();
								String message[] = result.getType().split("@@");
								if(message[0].equals("success")){
									MainFrame window = new MainFrame(frame,(User)result.getObject());
									frame.setVisible(false);
									window.frame.setVisible(true);
									break;
								}else{
									label_error_tip.setText(message[1]);
									return;
								}
							
							}
							
							
						} catch (Exception e) {
							e.printStackTrace();
						}finally {
							try{
								if(socket.isConnected()){
									socket.close();
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				});
			}
		});
		btn_login.setBounds(280, 205, 93, 23);
		frame.getContentPane().add(btn_login);
		
		//按钮---注册
		JButton btn_register = new JButton("\u6CE8\u518C");
		btn_register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							UserRegister window = new UserRegister();
							window.frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btn_register.setBounds(52, 205, 93, 23);
		frame.getContentPane().add(btn_register);
		
		//标签---错误提示标签
		label_error_tip = new JLabel("");
		label_error_tip.setFont(new Font("宋体", Font.PLAIN, 12));
		label_error_tip.setForeground(Color.RED);
		label_error_tip.setBounds(167, 132, 172, 28);
		frame.getContentPane().add(label_error_tip);
	}
}

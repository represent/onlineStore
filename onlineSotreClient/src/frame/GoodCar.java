package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.eclipse.osgi.internal.permadmin.SecurityTable;

import model.Good;
import model.OrderModel;
import model.SendModel;
import model.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JToggleButton;

public class GoodCar {

	JFrame frame;
	private DefaultTableModel model;
	private JTable table; 
	private JScrollPane  scrollPane;
	private User currentUser;
	private double totalPrice;
	private int accountNum;
	private JLabel label_acountNum; 
	private JLabel label_totalPrice;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public GoodCar(User currentUser) {
		this.currentUser = currentUser;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setEnabled(false);
		frame.setTitle("\u8D2D\u7269\u8F66");
		frame.setBounds(100, 100, 590, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		//标签---当前用户名-提示
		JLabel label_userNameTip = new JLabel("\u7528  \u6237\uFF1A");
		label_userNameTip.setBounds(10, 21, 54, 15);
		frame.getContentPane().add(label_userNameTip);
		
		//标签---当前用户名
		JLabel label_userName = new JLabel("New label");
		label_userName.setEnabled(false);
		label_userName.setBounds(93, 21, 89, 15);
		label_userName.setText(currentUser.getUserName());
		frame.getContentPane().add(label_userName);
		
		//标签---时间-提示
		JLabel label_time = new JLabel("\u65F6  \u95F4\uFF1A");
		label_time.setBounds(318, 21, 54, 15);
		frame.getContentPane().add(label_time);
		
		//标签---时间-年
		JLabel label_time_year = new JLabel("1997");
		label_time_year.setEnabled(false);
		label_time_year.setBounds(382, 21, 36, 15);
		frame.getContentPane().add(label_time_year);
		
		//标签---时间-年-提示
		JLabel label_time_yearTip = new JLabel("\u5E74");
		label_time_yearTip.setBounds(428, 21, 18, 15);
		frame.getContentPane().add(label_time_yearTip);
		
		//标签---时间-月
		JLabel label_time_month = new JLabel("01");
		label_time_month.setEnabled(false);
		label_time_month.setBounds(456, 21, 18, 15);
		frame.getContentPane().add(label_time_month);
		
		//标签---时间-月-提示
		JLabel label_time_monthTip = new JLabel("\u6708");
		label_time_monthTip.setBounds(484, 21, 18, 15);
		frame.getContentPane().add(label_time_monthTip);
		
		//标签---时间-天
		JLabel label_time_day = new JLabel("01");
		label_time_day.setEnabled(false);
		label_time_day.setBounds(512, 21, 18, 15);
		frame.getContentPane().add(label_time_day);
		
		//标签---时间-天-提示
		JLabel label_time_dayTip = new JLabel("\u65E5");
		label_time_dayTip.setBounds(540, 21, 24, 15);
		frame.getContentPane().add(label_time_dayTip);
		//启动一个线程 去实时更新 时间
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true){
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						// TODO: handle exception
					}
					Calendar calendar = Calendar.getInstance();
					label_time_year.setText(""+calendar.get(Calendar.YEAR));
					label_time_month.setText(""+calendar.get(Calendar.MONTH));
					label_time_day.setText(""+calendar.get(Calendar.DAY_OF_MONTH));
					
				}
				
			}
		}).start();
		
		model = new DefaultTableModel();
		model.addColumn("商品编号");
		model.addColumn("名称");
		model.addColumn("单价(人民币)");
		model.addColumn("库存");
		setTable(currentUser.getGoodCar());
		table = new JTable( model );
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 91, 554, 192);
		frame.getContentPane().add(scrollPane);
		
		//标签---总数量-提示
		JLabel label_acountNumTip = new JLabel("\u603B \u6570 \u91CF\uFF1A");
		label_acountNumTip.setBounds(10, 293, 74, 15);
		frame.getContentPane().add(label_acountNumTip);
		
		//标签---货物总数量
		label_acountNum = new JLabel("0");
		label_acountNum.setEnabled(false);
		label_acountNum.setBounds(93, 293, 54, 15);
		label_acountNum.setText(""+accountNum);
		frame.getContentPane().add(label_acountNum);
		
		//标签---货物总金额-提示
		JLabel label_totalPriceTip = new JLabel("\u603B \u91D1 \u989D\uFF1A");
		label_totalPriceTip.setBounds(10, 333, 74, 15);
		
		frame.getContentPane().add(label_totalPriceTip);
		
		//标签---货物总金额
		label_totalPrice = new JLabel("0");
		label_totalPrice.setEnabled(false);
		label_totalPrice.setBounds(93, 333, 54, 15);
		label_totalPrice.setText(""+totalPrice);
		frame.getContentPane().add(label_totalPrice);
		
		//按钮---支付
		JButton btn_pay = new JButton("\u7ED3\u8D26");
		btn_pay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try(
					Socket socket = new Socket("127.0.0.1",9999);
					ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				){
					SendModel sendModel = new SendModel();
					sendModel.setType("pay");
					OrderModel orderModel = new OrderModel();
					orderModel.setUserName(currentUser.getUserName());
					sendModel.setObject(orderModel);
					oos.writeObject(sendModel);
					
					SendModel result = (SendModel)ois.readObject();
					String message[] = result.getType().split("@@");
					if(message[0].equalsIgnoreCase("success")){
						currentUser = (User)result.getObject();
						MainFrame.currentUser= currentUser;
						JOptionPane.showMessageDialog(null,
								"结账成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
					}else{
						JOptionPane.showMessageDialog(null,
								"结账失败", "系统信息", JOptionPane.INFORMATION_MESSAGE);
					}
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btn_pay.setBounds(449, 308, 93, 23);
		frame.getContentPane().add(btn_pay);
		//按钮---删除
		JButton button_delete = new JButton("\u5220\u9664");
		button_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectRow = table.getSelectedRow();
				if(selectRow!=-1){
					String name = (String)table.getValueAt(selectRow,1);
					try(
						Socket socket = new Socket("127.0.0.1",9999);
						ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
						ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
					){
						SendModel  sendModel = new SendModel();
						sendModel.setType("deleteGood");
						OrderModel orderModel = new OrderModel();
						orderModel.setGoodName(name);
						orderModel.setUserName(currentUser.getUserName());
						sendModel.setObject(orderModel);
						oos.writeObject(sendModel);
						SendModel result  = (SendModel)ois.readObject();
						String message[] = result.getType().split("@@");
						if(message[0].equalsIgnoreCase("success")){
							currentUser=(User)result.getObject();
							MainFrame.currentUser=currentUser;
							JOptionPane.showMessageDialog(null,
									"删除成功", "系统信息", JOptionPane.INFORMATION_MESSAGE);
						}
					}catch(Exception e1){
						e1.printStackTrace();
					}
				}else{
					JOptionPane.showMessageDialog(null,
							"未选中商品，请双击选中", "系统信息", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		button_delete.setBounds(325, 308, 93, 23);
		frame.getContentPane().add(button_delete);
		
		//按钮---购物记录
		JToggleButton toggleBtn_record = new JToggleButton("\u8D2D\u7269\u8F66");
		toggleBtn_record.setBounds(10, 55, 135, 23);
		toggleBtn_record.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(!toggleBtn_record.isSelected()){
					toggleBtn_record.setText("购物车");
					setTable(currentUser.getGoodCar());
					btn_pay.setVisible(true);
					button_delete.setVisible(true);
					//设定货物总数量
					label_acountNum.setText(""+accountNum);
					//设定总金额
					label_totalPrice.setText(""+totalPrice);
				}else{
					toggleBtn_record.setText("购物记录");
					setTable(currentUser.getGoodRecord());
					btn_pay.setVisible(false);
					button_delete.setVisible(false);
					//设定货物总数量
					label_acountNum.setText(""+accountNum);
					//设定总金额
					label_totalPrice.setText(""+totalPrice);
				}
			}
		});
		frame.getContentPane().add(toggleBtn_record);
		
	}
	public void setTable(Map<Good, Integer> goodMap){
		accountNum=0;
		totalPrice = 0;
		//清空 model
		int length = model.getRowCount();
		for(int i = length-1;i>=0;i--){
			model.removeRow(i);
		}
		//为model添加数据
		goodMap.keySet().forEach((good)->{
			int num =goodMap.get(good);
			Object [] object = new Object[4];
			object[0]=good.getGoodId();
			object[1]=good.getGoodName();
			object[2]=good.getGoodPrice();
			object[3]=num;
			totalPrice = totalPrice+ good.getGoodPrice()*num;
			accountNum+=num;
			model.addRow(object);
		});
		
	}
}

package frame;

import java.awt.EventQueue;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.SendModel;
import model.User;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class UserRegister {

	JFrame frame;
	private JTextField tf_userName;
	private JPasswordField tf_passwd;
	private JPasswordField tf_passwd2;
	private JLabel label_error_tip;
	private Socket socket;
	
	

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public UserRegister(){
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u7528\u6237\u6CE8\u518C");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//标签---用户名
		JLabel label_userName = new JLabel("\u7528\u6237\u540D\u79F0\uFF1A");
		label_userName.setBounds(55, 66, 77, 15);
		frame.getContentPane().add(label_userName);
		
		//标签---密码
		JLabel label_passwd = new JLabel("\u5BC6    \u7801\uFF1A");
		label_passwd.setBounds(55, 108, 77, 15);
		frame.getContentPane().add(label_passwd);
		
		//标签---确认密码
		JLabel label_passwd2 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		label_passwd2.setBounds(55, 151, 77, 15);
		frame.getContentPane().add(label_passwd2);
		
		//文本框---用户名
		tf_userName = new JTextField();
		tf_userName.setBounds(172, 63, 173, 21);
		frame.getContentPane().add(tf_userName);
		tf_userName.setColumns(10);
		
		//文本框---密码
		tf_passwd = new JPasswordField();
		tf_passwd.setBounds(172, 105, 173, 21);
		frame.getContentPane().add(tf_passwd);
		tf_passwd.setColumns(10);
		
		//文本框---确认密码
		tf_passwd2 = new JPasswordField();
		tf_passwd2.setBounds(172, 148, 173, 21);
		frame.getContentPane().add(tf_passwd2);
		tf_passwd2.setColumns(10);
		
		//按钮---注册
		JButton btn_register = new JButton("\u6CE8\u518C");
		btn_register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						try {
							socket = new Socket("127.0.0.1",9999);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						//获取文本框信息
						String userName = tf_userName.getText().trim();
						String passwd1 = new String(tf_passwd.getPassword());
						String passwd2 = new String(tf_passwd2.getPassword());
						
						//基本逻辑判断
						if(userName==null||userName.equals("")){
							label_error_tip.setText("[-> 用户名不为空 <-]");
							return;
						}else if(passwd1==null||passwd1.equals("")){
							label_error_tip.setText("[-> 密码不为空 <-]");
							return;
						}else if(passwd2==null||passwd2.equals("")){
							label_error_tip.setText("[-> 密码不为空 <-]");
							return;
						}
						if(!passwd1.equals(passwd2)){
							label_error_tip.setText("[-> 两次密码不相同 <-]");
							return;
						}
						//发送信息
							try(
								
								ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
								ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
							){
								
								User user = new User();
								user.setUserName(userName);
								user.setUserPass(passwd1);
								SendModel model = new SendModel();
								model.setType("register");
								model.setObject(user);
								oos.writeObject(model);
								
								SendModel result = (SendModel)ois.readObject();
								String[] message = result.getType().split("@@");
								JOptionPane.showMessageDialog(null,
										message[1], "系统信息", JOptionPane.INFORMATION_MESSAGE);
								
							}catch(Exception e1){
								e1.printStackTrace();
							}finally {
								try{
									if(socket.isConnected()){
										socket.close();
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						
					}
				});
			}
		});
		btn_register.setBounds(275, 204, 93, 23);
		frame.getContentPane().add(btn_register);
		
		//标签---错误信息提示
		label_error_tip = new JLabel("");
		label_error_tip.setFont(new Font("宋体", Font.PLAIN, 12));
		label_error_tip.setForeground(Color.RED);
		label_error_tip.setBounds(134, 25, 191, 15);
		frame.getContentPane().add(label_error_tip);
	}
}
